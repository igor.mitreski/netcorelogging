﻿using System.IO;
using System.Reflection;
using Microsoft.Extensions.Logging;
using log4net;
using log4net.Config;

namespace NetCoreLogging.Database
{
	public class LoggerBase
	{
		private readonly ILogger _logger;

		protected LoggerBase()
		{
		}

		protected LoggerBase(ILoggerFactory loggerFactory)
		{
			var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
			XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
			_logger = loggerFactory.CreateLogger("NetCoreLogging");
		}

		//public virtual void LogError(ILogEntry logEntry)
		//{
		//	if (_logger != null)
		//	{
		//		LogicalThreadContext.Properties["Payload"] = logEntry.Payload;
		//		LogicalThreadContext.Properties["RequestToken"] = logEntry.RequestToken;
		//		LogicalThreadContext.Properties["ResponseToken"] = logEntry.ResponseToken;
		//		LogicalThreadContext.Properties["UserToken"] = logEntry.UserToken;

		//		_logger.LogError(logEntry.Exception, logEntry.Message, logEntry.Exception);
		//	}
		//}
	}
}
