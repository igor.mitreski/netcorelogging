using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using log4net;
using log4net.Config;

namespace NetCoreLogging.Database
{
	public class Program
	{
		public static void Main(string[] args)
		{
			CreateHostBuilder(args).Build().Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureLogging(logging => 
				{
					var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
					XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
				})
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseStartup<Startup>();
				});
	}
}
