﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace NetCoreLogging.Database.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class DatabaseLoggingController : ControllerBase
	{
		private readonly ILogger<DatabaseLoggingController> _logger;

		public DatabaseLoggingController(ILogger<DatabaseLoggingController> logger)
		{
			_logger = logger;
		}

		[HttpGet("Warning")]
		public int Warning()
		{
			var rng = new Random();
			var randomValue = rng.Next();

			_logger.LogWarning("This is random log warning");

			//FlushBuffers();

			return randomValue;
		}

		[HttpGet("Info")]
		public int Info()
		{
			var rng = new Random();
			var randomValue = rng.Next();

			_logger.LogInformation("This is random log information");

			//FlushBuffers();

			return randomValue;
		}

		[HttpGet("Error")]
		public int Error()
		{
			var rng = new Random();
			var randomValue = rng.Next();

			_logger.LogError("This is random log error");

			//FlushBuffers();

			return randomValue;
		}

		//public void FlushBuffers()
		//{
		//	ILoggerRepository rep = LogManager.GetRepository();
		//	foreach (IAppender appender in rep.GetAppenders())
		//	{
		//		var buffered = appender as BufferingAppenderSkeleton;
		//		if (buffered != null)
		//		{
		//			buffered.Flush();
		//		}
		//	}
		//}
	}
}
