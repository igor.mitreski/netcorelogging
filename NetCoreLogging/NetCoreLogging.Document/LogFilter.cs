﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;
using log4net;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace NetCoreLogging.Document
{
	public class LogFilter : ActionFilterAttribute, IAsyncActionFilter
	{
		#region Fields

		private readonly ILog _loger;

		#endregion

		#region Constructors

		public LogFilter()
		{
			//TODO: hack...this goes to Program.cs
			var config = new XmlDocument();
			config.Load(File.OpenRead(@"log4net.config"));
			var repo = LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
			log4net.Config.XmlConfigurator.Configure(repo, config["log4net"]);

			_loger = LogManager.GetLogger(typeof(LogFilter));
		}

		#endregion


		#region IAsyncActionFilter

		public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
		{
			var request = context.HttpContext.Request;

			var url = $"{request.Host}{request.Path}";
			var method = request.Method;

			var arguments = context.ActionArguments.FirstOrDefault(x => x.Key != "Id");
			var payload = JsonConvert.SerializeObject(arguments.Value);

			var payloadString = arguments.Value is null ? string.Empty : $"REQUEST: {payload}";

			var logMessage = $"[{method}] {url} {payloadString}";

			_loger.Info(logMessage);

			await next();
		}

		#endregion

		#region ActionFilterAttribute

		public override async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
		{
			var request = context.HttpContext.Request;

			var url = $"{request.Host}{request.Path}";
			var method = request.Method;
			var payload = JsonConvert.SerializeObject(context.Result);

			var payloadString = $"RESPONSE: {payload}";

			var logMessage = $"[{method}] {url} {payloadString}";

			_loger.Info(logMessage);

			await next();
		}

		#endregion
	}
}
