﻿using log4net;
using log4net.Config;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Reflection;

namespace NetCoreLogging.Document
{
	public class LoggerBase
	{
		private readonly ILogger _logger;

		protected LoggerBase()
		{
		}

		protected LoggerBase(ILoggerFactory loggerFactory)
		{
			var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
			XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
			_logger = loggerFactory.CreateLogger("NetCoreLogging");
		}
	}
}
