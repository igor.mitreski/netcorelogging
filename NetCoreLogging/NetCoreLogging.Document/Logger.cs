﻿using Microsoft.Extensions.Logging;

namespace NetCoreLogging.Document
{
	public class Logger : LoggerBase
	{

		public Logger()
			: base()
		{

		}

		public Logger(ILoggerFactory logger)
			: base(logger)
		{

		}
	}
}
