﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace NetCoreLogging.Document.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class LoggingController : ControllerBase
	{
		private readonly ILogger<LoggingController> _logger;

		public LoggingController(ILogger<LoggingController> logger)
		{
			_logger = logger;
		}

		[HttpGet("Warning")]
		public int Warning()
		{
			var rng = new Random();
			var randomValue = rng.Next();

			_logger.LogWarning("This is random log warning", new { randomValue });

			return randomValue;
		}

		[HttpGet("Info")]
		public int Info()
		{
			var rng = new Random();
			var randomValue = rng.Next();

			_logger.LogInformation("This is random log information", new { randomValue });

			return randomValue;
		}

		[HttpGet("Error")]
		public int Error()
		{
			var rng = new Random();
			var randomValue = rng.Next();

			_logger.LogError("This is random log error", new { randomValue });

			return randomValue;
		}
	}
}
