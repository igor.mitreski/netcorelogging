﻿using Microsoft.Extensions.Logging;

namespace NetCoreLogging.Console
{
	class Program
	{
		static void Main(string[] args)
		{
			var loggerFactory = LoggerFactory.Create(builder => {
				builder.AddFilter("Microsoft", LogLevel.Warning)
					   .AddFilter("System", LogLevel.Warning)
					   .AddFilter("NetCoreLogging.Console", LogLevel.Warning)
					   .AddConsole();
			});

			var logger = loggerFactory.CreateLogger<Program>();

			logger.LogTrace("This is trace log message.");

			logger.LogInformation("This is info log message.");

			logger.LogDebug("This is debug log message.");

			logger.LogWarning("This is warning log message.");

			logger.LogError("This is error log message.");

			logger.LogCritical("This is critical log message.");
		}
	}
}
